const invalidRequest = (method, url) => {
  return {
    requestMethod: method,
    requestUrl: url,
    allowedRequests: [
      `GET /readiness`,
      `GET /api/v1/inventory`,
      `POST /api/v1/inventory`
    ]
  };
};

function notFound_404(req, res) {
  const { method, url } = req;
  const resBody = JSON.stringify(invalidRequest(method, url));

  res.writeHead(404, {
    'Content-Type': 'text/html',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = notFound_404;
