function readinessProbe(req, res) {
  const resBody = JSON.stringify({ isReady: true });

  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = readinessProbe;
