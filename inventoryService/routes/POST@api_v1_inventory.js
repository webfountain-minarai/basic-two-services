// Returns a random number between min (inclusive) and max (exclusive)
function randomNumberGenerator() {
  const min = 1000;
  const max = 10000;

  return Math.floor(
    Math.random() * (max - min) + min
  )
}


function postApiInventory(req, res) {
  const itemId = randomNumberGenerator();
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', () => {
    reqBody = Buffer.concat(reqBody).toString();
    const { sellerId, title } = JSON.parse(reqBody);

    global.state.push({ sellerId, itemId, title });

    res.writeHead(201, { 'Content-Type': 'application/json' });
    res.end();
  });
}


module.exports = postApiInventory;
