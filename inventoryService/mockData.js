module.exports = [
  {
    sellerId: 1234,
    itemId: 859921,
    title: 'coffee bulb'
  },
  {
    sellerId: 2345,
    itemId: 238740,
    title: 'nauvoo'
  },
  {
    sellerId: 3456,
    itemId: 603721,
    title: 'engine cone'
  }
];
