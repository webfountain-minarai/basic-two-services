const getHome = require('./GET@');
const postHome = require('./POST@');
const getInventoryService = require('./GET@inventory-service');
const postInentoryService = require('./POST@inventory-service');
const readinessProbe = require('./readinessProbe');
const notFound_404 = require('./notFound_404');


const allowedMethods = ['GET', 'POST'];
const allowedPaths = ['/', '/inventory-service', '/readiness'];
const allowedGETpaths = ['/', '/inventory-service', '/readiness'];
const allowedPOSTpaths = ['/', '/inventory-service'];

function routes() {
  return (req, res) => {
    const { method, url } = req;

    console.group('Route Handler - Front Office Service');
    console.info({ method, url })
    console.groupEnd();

    if (!allowedMethods.includes(method) || !allowedPaths.includes(url) ||
        (method === 'GET' && !allowedGETpaths.includes(url)) ||
        (method === 'POST' && !allowedPOSTpaths.includes(url))
    ) notFound_404(req, res);

    if (method === 'GET' && url === '/') getHome(req, res);

    if (method === 'POST' && url === '/') postHome(req, res);

    if (method === 'GET' && url === '/inventory-service') getInventoryService(req, res);

    if (method === 'POST' && url === '/inventory-service') postInentoryService(req, res);

    if (method === 'GET' && url === '/readiness') readinessProbe(req, res);
  };
}


module.exports = routes;
