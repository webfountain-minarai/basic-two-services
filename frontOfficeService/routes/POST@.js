// Returns a random number between min (inclusive) and max (exclusive)
function randomNumberGenerator() {
  const min = 1000;
  const max = 10000;

  return Math.floor(
    Math.random() * (max - min) + min
  )
}


function postHome(req, res) {
  const sellerId = randomNumberGenerator();
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', () => {
    reqBody = Buffer.concat(reqBody).toString();
    const { name, company } = JSON.parse(reqBody);

    global.state.push({ sellerId, name, company });

    const resBody = JSON.stringify({
      message: `Thank you ${name} for contacting the Front Office Service. Your request has been processed. ${company} seller id is ${sellerId}.`
    });

    res.writeHead(201, {
      'Content-Length': Buffer.byteLength(resBody),
      'Content-Type': 'application/json'
    });
    res.write(resBody);
    res.end();
  });
}


module.exports = postHome;
