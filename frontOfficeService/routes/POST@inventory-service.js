const http = require('http');


function postInventoryService(req, res) {
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', () => {
    reqBody = Buffer.concat(reqBody).toString();
    const { sellerId, title } = JSON.parse(reqBody);

    const options = {
      hostname: process.env.INVENTORY_SERVICE_HOST || 'localhost',
      port: process.env.INVENTORY_SERVICE_PORT || 3100,
      path: '/api/v1/inventory',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(reqBody)
      }
    };

    const postRequest = http.request(options, resp => {
      console.group('FOS - POST /Inventory-service Response');
      console.log(`STATUS: ${resp.statusCode}`);
      console.log(`HEADERS: ${JSON.stringify(resp.headers)}`);
      console.groupEnd();

      const resBody = JSON.stringify({
        message: `Sucessfully added ${title} to sellerId ${sellerId} inventory`
      });

      res.writeHead(200, {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(resBody)
      });
      res.write(resBody);
      res.end()
    });

    postRequest.write(reqBody)
    postRequest.end();
  });
}


module.exports = postInventoryService;
