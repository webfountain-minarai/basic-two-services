module.exports = [
  {
    sellerId: 1234,
    name: 'James "Jim" R. Holden',
    company: 'Rocinante'
  },
  {
    sellerId: 2345,
    name: 'Frederick "Fred" Lucius Johnson',
    company: 'Tycho Station'
  },
  {
    sellerId: 3456,
    name: 'Solomon Epstein',
    company: 'Epstein-Fusion Drive'
  }
];
